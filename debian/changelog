eaphammer (1.14.0-0kali2) kali-dev; urgency=medium

  * d/rules: Use Config and not config for openssl build

 -- Steev Klimaszewski <steev@kali.org>  Wed, 06 Dec 2023 14:02:30 -0600

eaphammer (1.14.0-0kali1) kali-dev; urgency=medium

  * New upstream version 1.14.0
  * Refresh patches

 -- Steev Klimaszewski <steev@kali.org>  Thu, 19 Oct 2023 15:30:34 -0500

eaphammer (1.13.5+git20201214-0kali5) kali-dev; urgency=medium

  * Sed in the proper march for armhf to fix build with gcc-11

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 16 Aug 2023 21:52:02 +0700

eaphammer (1.13.5+git20201214-0kali4) kali-dev; urgency=medium

  [ Ben Wilson ]
  * Consistency with tabs to spaces
  * Remove template comment and switch spaces to tabs

  [ Kali Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.
  * Update standards version to 4.6.2, no changes needed.

  [ Steev Klimaszewski ]
  * Add lintian overrides.
  * Add patch for host determination.
  * d/rules: Update path to patch
  * Attempt to apply patch with p1
  * Make host check more generic
  * Different take on the patch
  * Drop patch and add verbosity
  * Actually add the verbosity and drop rules patching
  * Test to pass arch.

  [ Arnaud Rebillout ]
  * Revert "Test to pass arch."
  * Add workaround for python3-eventlet

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 16 Aug 2023 19:58:18 +0700

eaphammer (1.13.5+git20201214-0kali3) kali-dev; urgency=medium

  * Clean debian/rules
  * Exclude libhostapd-eaphammer.so from dh_python3

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 12 Aug 2021 14:03:53 +0200

eaphammer (1.13.5+git20201214-0kali2) kali-dev; urgency=medium

  * Make the build more verbose
  * Download a more recent openssl tarball to fix the build failure on arm64:
    "relocation R_AARCH64_PREL64 against symbol `OPENSSL_armcap_P' which may
    bind externally can not be used when making a shared object"
  * Improve package removal: remove correctly the non-empty directories

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 10 Aug 2021 17:41:45 +0200

eaphammer (1.13.5+git20201214-0kali1) kali-dev; urgency=medium

  * Initial release (see 6565)

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 13 Jul 2021 11:03:32 +0200
